package com.nttdata.page;

import org.openqa.selenium.By;

public class CalculatorPage {
    //localizadores

    public static By numero1Input = By.id("number1Field");
    public static By numero2Input = By.id("number2Field");
    public static By selectorInput = By.id("selectOperationDropdown");
    public static By calculaButton = By.id("calculateButton");
    public static By resultadoInput = By.id("numberAnswerField");

    public static By clearButton = By.id("clearButton");
}
