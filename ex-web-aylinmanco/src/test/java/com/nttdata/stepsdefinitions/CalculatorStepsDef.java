package com.nttdata.stepsdefinitions;

import com.nttdata.steps.CalculatorSteps;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class CalculatorStepsDef {
    private WebDriver driver;
    private Scenario scenario;

    private CalculatorSteps calculatorSteps(WebDriver driver){
        return new CalculatorSteps(driver);
    }

    @Before(order = 0)
    public void setUp(){
        System.setProperty("webdriver.chrome.driver","C:\\Users\\amancoar\\Proyectos\\Git\\ex-web-aylinmanco\\drivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }
    @Before(order = 1)
    public void setScenario(Scenario scenario){
        this.scenario = scenario;
    }
    @After
    public void quitDriver(){
        driver.quit();
    }

    @Dado("que me encuentro en la página de calculator")
    public void que_me_encuentro_en_la_página_de_calculator() {
    driver.get("https://testsheepnz.github.io/BasicCalculator.html");
    screenShot();
    }
    @Cuando("ingreso el firstnumber: {string}, secondnumber: {string}")
    public void ingreso_el_firstnumber_secondnumber(String numero1, String numero2) {
        CalculatorSteps calculatorSteps = new CalculatorSteps(driver);
        calculatorSteps.typeNumero1(numero1);
        calculatorSteps.typeNumero2(numero2);
        screenShot();
    }
    @Cuando("selecciono un operador {string}")
    public void selecciono_un_operador(String selector) {
        CalculatorSteps calculatorSteps = new CalculatorSteps(driver);
        calculatorSteps.selectorInput(selector);
        calculatorSteps.calculaButton();
        screenShot();
    }

    @Entonces("valido que debería mostrar el resultado de la suma {string}")
    public void valido_que_debería_mostrar_el_resultado_de_la_suma(String resultado) {
        String suma =  calculatorSteps(driver).resultado();
        Assertions.assertEquals(resultado, suma);
        screenShot();
    }

    @Entonces("limpiar con el boton {string}")
    public void limpiar_con_el_boton(String string) {
        CalculatorSteps calculatorSteps = new CalculatorSteps(driver);
        calculatorSteps.limpiarBoton();
    }
    @Entonces("valido que debería mostrar el resultado de la resta {string}")
    public void valido_que_debería_mostrar_el_resultado_de_la_resta(String resultado) {
        String resta =  calculatorSteps(driver).resultado();
        Assertions.assertEquals(resultado, resta);
        screenShot();
    }


    @Entonces("valido que debería mostrar el resultado de la multiplicacion {string}")
    public void valido_que_debería_mostrar_el_resultado_de_la_multiplicacion(String resultado) {
        String multiplicacion =  calculatorSteps(driver).resultado();
        Assertions.assertEquals(resultado, multiplicacion);
        screenShot();
    }
    @Entonces("valido que debería mostrar el resultado de la division {string}")
    public void valido_que_debería_mostrar_el_resultado_de_la_division(String resultado) {
        String division =  calculatorSteps(driver).resultado();
        Assertions.assertEquals(resultado, division);
        screenShot();
    }

    @Entonces("valido que debería mostrar el resultado de la concatenacion {string}")
    public void valido_que_debería_mostrar_el_resultado_de_la_concatenacion(String resultado) {
        String concatenacion =  calculatorSteps(driver).resultado();
        Assertions.assertEquals(resultado, concatenacion);
        screenShot();
    }



    private void screenShot() {
        byte[] evidencia = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
        this.scenario.attach(evidencia, "image/png", "evidencias");
    }

}
