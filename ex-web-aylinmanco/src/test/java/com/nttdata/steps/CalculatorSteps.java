package com.nttdata.steps;

import com.nttdata.page.CalculatorPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CalculatorSteps {
    private WebDriver driver;

    public CalculatorSteps(WebDriver driver) {
        this.driver = driver;
    }

    public void typeNumero1(String numero1) {
        WebElement numero1InputElement = driver.findElement(CalculatorPage.numero1Input);
        numero1InputElement.sendKeys(numero1);
    }

    public void typeNumero2(String numero2) {
        WebElement numero1InputElement = driver.findElement(CalculatorPage.numero2Input);
        numero1InputElement.sendKeys(numero2);
    }

    public void selectorInput(String selector) {
        WebElement selectorInputElement = driver.findElement(CalculatorPage.selectorInput);
        selectorInputElement.sendKeys(selector);
    }

    public void calculaButton() {
        this.driver.findElement(CalculatorPage.calculaButton).click();
    }

    public String resultado() {
        return this.driver.findElement(CalculatorPage.resultadoInput).getText();
    }

    public void limpiarBoton(){
        this.driver.findElement(CalculatorPage.clearButton).click();
    }


}
