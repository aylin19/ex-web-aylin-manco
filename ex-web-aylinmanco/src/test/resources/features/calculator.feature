#language: es

@testfeature
Característica: Página calculator
  Yo, como usuario
  Quiero, realizar operaciones matemáticas
  Para ver los resultados

  @test1
  Escenario: Realizar suma
    Dado que me encuentro en la página de calculator
    Cuando ingreso el firstnumber: "3", secondnumber: "4"
    Y selecciono un operador "add"
    Entonces valido que debería mostrar el resultado de la suma ""
    Y limpiar con el boton "clear"

  @test2
  Escenario: Realizar resta
    Dado que me encuentro en la página de calculator
    Cuando ingreso el firstnumber: "4", secondnumber: "3"
    Y selecciono un operador "subtract"
    Entonces valido que debería mostrar el resultado de la resta ""
    Y limpiar con el boton "clear"

  @test3
  Escenario: Realizar multiplicación
    Dado que me encuentro en la página de calculator
    Cuando ingreso el firstnumber: "3", secondnumber: "4"
    Y selecciono un operador "multiply"
    Entonces valido que debería mostrar el resultado de la multiplicacion ""
    Y limpiar con el boton "clear"

  @test4
  Escenario: Realizar división
    Dado que me encuentro en la página de calculator
    Cuando ingreso el firstnumber: "10", secondnumber: "5"
    Y selecciono un operador "divide"
    Entonces valido que debería mostrar el resultado de la division ""
    Y limpiar con el boton "clear"

  @test5
  Escenario: Realizar concatenación
    Dado que me encuentro en la página de calculator
    Cuando ingreso el firstnumber: "3", secondnumber: "4"
    Y selecciono un operador "concatenate"
    Entonces valido que debería mostrar el resultado de la concatenacion ""
    Y limpiar con el boton "clear"
